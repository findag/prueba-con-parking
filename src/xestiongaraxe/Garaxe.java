
package xestiongaraxe;


public class Garaxe
{
    final private float prezoInicial = 0.0295f;
    final private float prezoSecundario = 0.0269f;
    final private int capacidade = 3;
    private int numeroCoches;
    private int matricula;
    private String marca;
    /**
     * Creamos 3 variables de tipo int matricula con distintos nomes para cubrir as 3 prazas
     */
    int matricula1;
    int matricula2;
    int matricula3;
    /**
     * Creamos as variables que usaremos no temporizador e en Conta.
     */
    long entra;
    long sae;
    long tempoTotal;
    float prezoTotal;
    
    /**
     * Creamos os constructores.
     */
    public void Garaxe()
    {
        
    }
    public void Garaxe(int numeroCoches, int matricula, String marca)
    {
        this.setNumeroCoches(numeroCoches);
        this.setMatricula(matricula);
        this.setMarca(marca);
    }
    /**
     * 
     * Creamos os Getes e Setes. 
     */
    public int getCapacidade() {
        return capacidade;
    }

    public int getNumeroCoches() {
        return numeroCoches;
    }

    public void setNumeroCoches(int numeroCoches) {
        this.numeroCoches = numeroCoches;
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
    /**
     * 
     * Creamos metodos 
     */
    public int TemosSitio()
    {
        return numeroCoches++;
    }
    public void NonTemosSitio()
    {
        System.out.println("COMPLETO");
    }
    public int Retirar()
    {
        return numeroCoches--;
        
    }
    public void Cartel()
    {
        System.out.println("Neste momento temos dispoñibles\n\t" + (capacidade - numeroCoches) + " plazas");
    }
    public void EntradaMatriculas(int matricula)
    {
        if (matricula1 == 0)
        {
            matricula1 = matricula;
            entra = System.currentTimeMillis();
        }
        else if (matricula2 == 0)
        {
            matricula2 = matricula;
            entra = System.currentTimeMillis();
        }
        else if (matricula3 == 0)
        {
            matricula3 = matricula;
            entra = System.currentTimeMillis();
        }
    }
    /**
     * 
     * Creamos o metodo Temporizador no que contaremos o tempo dos coches dentro do parking.
     */
    public void Temporizador(int matricula)
    {
        if (matricula1 == matricula)
        {           
            sae = System.currentTimeMillis();
        }
        else if (matricula2 == matricula)
        {
            sae = System.currentTimeMillis();
        }
        else if (matricula3 == matricula)
        {
            sae = System.currentTimeMillis();
        }
    }
    public String Conta(long sae, long entra)
    {
        tempoTotal = (sae - entra)/1000;
        if (tempoTotal<=30)
        {
            prezoTotal = tempoTotal * prezoInicial;
        }
        else
        {
            prezoTotal = ((30 * prezoInicial) + ( tempoTotal-30) * prezoSecundario);
        }
        return ("O gasto do parking por " + tempoTotal + "minutos e " + prezoTotal);
    }
    
}
